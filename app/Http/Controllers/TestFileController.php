<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestFileController extends Controller
{
    public function index(){
        return view('test.index');
    }

    public function store(){
        $pathToUploadDir = 'C:\osPanel\domains\hillelblog.test\test_uploads\\';
        $uploadFile = $pathToUploadDir.date('Y-m-d'). $_FILES['product_photo']['name'];
        if(move_uploaded_file($_FILES['product_photo']['tmp_name'], $uploadFile)){
            dd('VSE OK');
        }else{
            dd('Problem occured');
        }

    }
}
