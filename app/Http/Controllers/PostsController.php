<?php

namespace App\Http\Controllers;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\PostsRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CreatePostsRequest;
use App\Http\Requests\UpdatePostsRequest;
use Symfony\Component\Console\Input\Input;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', [ 'except' => ['index','show']]);
    }

    public function index(){
        $posts = Post::all();
        $pageTitle = 'Super Blog page';
        return view('posts.index', compact('posts','pageTitle'));
    }

    public function show(Post $post){
        return view('posts.show', compact('post'));
    }

    public function create(){
        return view('posts.create');
    }

    public function store(PostsRequest $request){
        $postArray = $request->only(['title','slug','intro','body']);
        $postArray['user_id'] = Auth::user()->id;
        $uploadedFile = NULL;
        if($request->hasFile('image')){
            $file = $request->file('image');
            $uploadedFile = $file->move(public_path().'\\uploads\\posts\\',time().'-'.$file->getClientOriginalName());
        }
        $postArray['image_path'] = $uploadedFile->getBasename();
        Post::create($postArray);
        return redirect('/posts');
    }

    public function edit(Post $post){
        return view('posts.edit', compact('post'));
    }

    public function update(PostsRequest $request, Post $post){
        $postArray = $request->only(['title', 'slug', 'intro', 'body']);
        $postArray['user_id'] = Auth::user()->id;

        if($request->hasFile('image')){
            $file = $request->file('image');
            $uploadedFile = $file->move(public_path().'\\uploads\\posts\\',time().'-'.$file->getClientOriginalName());
            $postArray['image_path'] = $uploadedFile->getBasename();
        }
        $post->update($postArray);
        return redirect('/posts');
    }

    public function destroy(Post $post){
        $post->delete();
        return redirect('/posts');
    }
}
