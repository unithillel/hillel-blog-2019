<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Cart;

class CartController extends Controller
{
    public function store(Product $product){
        $cartArray = Cart::addProduct($product);
        return back()->withCookie('cart', json_encode($cartArray));
    }

    public function destroy(Product $product){
        $cartArray = Cart::removeProduct($product);
        return back()->withCookie('cart', json_encode($cartArray));
    }

    public function index(){
        $cartArray = Cart::getCartWithProducts();
        return view('cart.index', compact('cartArray'));
    }
}
