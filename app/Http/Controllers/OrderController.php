<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\Order;
use Illuminate\Http\Request;
use App\Cart;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function create(){
        $cartWithProducts = Cart::getCartWithProducts();
        return view('orders.create', compact('cartWithProducts'));
    }

    public function store(OrderRequest $request){
        $order = Order::create($request->all()+['user_id' => Auth::user()->id]);
        $cart = Cart::getCartArray();
        //attach Products to Order
        foreach ($cart as $product_id => $amount){
            $order->products()->attach($product_id, ['amount' => $amount]);
        }

        return redirect('products')->withCookie('cart', json_encode([]));
        //redirect to Products
    }
}
