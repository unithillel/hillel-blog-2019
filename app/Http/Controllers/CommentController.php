<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CommentsRequest;
use App\Http\Requests\CommentsUpdateRequest;

class CommentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(CommentsRequest $request)
    {
        Comment::create($request->all() + ['user_id' => Auth::user()->id]);
        return back()->with(['status' => 'Comment successfully created!']);
    }

    public function edit(Comment $comment)
    {
        return view('comments.edit', compact('comment'));
    }

    public function update(CommentsUpdateRequest $request, Comment $comment)
    {
        $comment->update($request->all() + ['user_id' => Auth::user()->id]);
        return redirect('/posts/'.$comment->post->slug)
            ->with(['status' => 'Comment successfully updated']);
    }

    public function destroy(Comment $comment)
    {
        $comment->delete();
        return back()->with(['status' => 'Comment successfully deleted!']);
    }
}
