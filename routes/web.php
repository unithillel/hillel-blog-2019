<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('homepage.index');
})->name('homepage');

Route::get('/contact-us', function(){
    return view('contacts.index');
})->name('contacts');


Route::get('/posts','PostsController@index');
// Displaying create form for Blog post
Route::get('/posts/create', 'PostsController@create');
// http://hillelblog.test/posts/3
Route::get('/posts/{post}','PostsController@show');
//Editing form for Blog Posts
Route::get('/posts/{post}/edit','PostsController@edit');

//Storing data from create Blog post form
Route::post('/posts', 'PostsController@store');

//Update blog post
Route::put('/posts/{post}','PostsController@update');

//Delete post
Route::delete('/posts/{post}', 'PostsController@destroy');

Route::resource('products', 'ProductController');

Route::resource('comments', 'CommentController')->except(['index', 'create', 'show']);
//Route::resource('comments', 'CommentController')->only(['create', 'update', 'edit', 'destroy']);

Auth::routes();

/** Cart routes: */
Route::get('/cart/{product}','CartController@store');
Route::get('/cart/{product}/delete','CartController@destroy');
Route::get('/cart','CartController@index');

/** Orders routes: */
Route::get('/order', 'OrderController@create');
Route::post('/order', 'OrderController@store');

/** == Admin panel routes: == */
Route::get('/admin/orders', 'Admin\OrderController@index');

/** File upload testing */
Route::get('/test', 'TestFileController@index');
Route::post('/test', 'TestFileController@store');
