<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::insert([
            [
                'title' => 'Php course',
                'slug' => 'php_course',
                'price' =>10,
                'description' => 'Super PHP Course'
            ],
            [
                'title' => 'JS course',
                'slug' => 'js_course',
                'price' => 11,
                'description' => 'Super JS Course'
            ],
        ]);
    }
}
