@extends('templates.basic')

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">File testing</h1>

        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <form action="/test" method="post" enctype="multipart/form-data">
                @csrf
                <label >Select image:
                    <br>
                    <input type="file" name="product_photo"></label>
                <br>
                <button class="btn btn-success">Save file</button>
            </form>
        </div>
    </div>
    <hr>
@endsection
