@extends('templates.basic')

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">{{$post->title}}</h1>
            <p>{{$post->intro}}</p>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div>
                <img src="/uploads/posts/{{$post->image_path}}" style="max-width: 1600px" class="rounded img-thumbnail">
            </div>
            <p>{{$post->body}}</p>
        </div>
    </div>
    <hr>
    <h2>Comments:</h2>
    <div class="row">

        <div class="col-md-12">
            <ul class="list-group">
                @foreach($post->comments as $comment)

                    <li class="list-group-item">
                        {{$comment->created_at->diffForHumans() }}
                        {{$comment->user->name}}
                        <strong>{{$comment->body}}</strong>
                        @if(Auth::check())
                            <a class="btn btn-warning" href="/comments/{{$comment->id}}/edit">Edit</a>

                            <form method="post" action="/comments/{{$comment->id}}">
                                @method('delete')
                                @csrf
                                <button class="btn btn-danger">X</button>
                            </form>
                        @endif
                    </li>

                @endforeach
            </ul>
        </div>
        @if(Auth::check())
            <div class="col-md-12">
                <form class="form-horizontal" method="post" action="/comments">
                    @csrf
                    <input type="hidden" name="post_id" value="{{$post->id}}">
                    <div class="form-group">
                        <label>Comment:
                            <textarea name="body" class="form-control"></textarea>
                        </label>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </form>
            </div>
        @endif
    </div>
@endsection
