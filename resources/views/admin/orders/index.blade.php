@extends('templates.basic')

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">Orders:</h1>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        @foreach($orders as $order)
            <div class="col-md-6">
                <h2>#{{$order->id}} - {{$order->user->email}}</h2>
                <p>Phone: {{$order->phone}}</p>
                <p>{{$order->comment}} </p>
                <ul>
                    @foreach($order->products as $product)
                        <li>
                            <strong>{{$product->id}}</strong>
                            {{$product->title}} x {{$product->pivot->amount}}
                        </li>
                    @endforeach
                </ul>
                <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
            </div>
        @endforeach
    </div>
    <hr>
@endsection
