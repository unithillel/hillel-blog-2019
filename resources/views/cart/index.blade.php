@extends('templates.basic')

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">Cart:</h1>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <tr>
                    <th> Product </th>
                    <th> Price </th>
                    <th> Amount </th>
                    <th> Total Price: </th>
                </tr>

                @foreach($cartArray as $cartItem )
                    <tr>
                        <td><strong>#{{$cartItem['product']->id}}</strong> - {{$cartItem['product']->title}}</td>
                        <td>{{$cartItem['product']->price}}</td>
                        <td>
                            <a href="/cart/{{$cartItem['product']->slug}}" class="btn btn-success">+</a>
                            {{$cartItem['amount']}}
                            <a href="/cart/{{$cartItem['product']->slug}}/delete" class="btn btn-danger">-</a>

                        </td>
                        <td>{{round($cartItem['product']->price * $cartItem['amount'], 2)}}</td>

                    </tr>
                @endforeach

            </table>

            <p>
                <strong><a href="/order">Create order</a></strong>
            </p>
        </div>
    </div>
@endsection
