@extends('templates.basic')

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">Products</h1>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        @foreach($products as $product)
            <div class="col-md-4">
                <h2>{{$product->title}}</h2>
                <p>{{$product->price}} </p>
                <p><a class="btn btn-secondary" href="/posts/{{$product->slug}}" role="button">View details &raquo;</a></p>
                @if(Auth::check())
                    <a href="/cart/{{$product->slug}}">Buy</a>
                @endif

            </div>
        @endforeach
    </div>
    <hr>
@endsection
